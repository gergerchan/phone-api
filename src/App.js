import Home from "./component/Home";
import {Container} from "reactstrap"
import ListBrands from "./component/ListBrands";
import ListPhones from "./component/ListPhones";
import PhoneSpecs from "./component/PhoneSpecs";

function App() {
  return (
    <Container>
      <Home />
      {/* <ListBrands /> */}
      {/* < ListPhones /> */}
      {/* < PhoneSpecs /> */}

    </Container>
  );
}

export default App;
