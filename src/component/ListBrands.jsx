import axios from "axios"
import { useEffect, useState } from "react"
import { Button } from "reactstrap"

const ListBrands = () => {
  const [listBrand, setListBrand] = useState(null)

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      const url = "http://api-mobilespecs.azharimm.tk/brands"
      await axios.get(url)
        .then(res => {
          // console.log(res);
          setListBrand(res.data.data.brands)
        })
    } catch (err) {
      console.log(err);
    }
  }
  console.log(listBrand);

  return (
    <>
      <h1>List Brands:</h1>
      {/* <Button onClick={getData}>GetData</Button> */}
      <ul>
        {listBrand && listBrand.map((data, i) => (
          <li key={i}>{data.brand}</li>
        ))}
      </ul>

    </>
  )
}

export default ListBrands