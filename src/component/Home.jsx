import React, {useState} from 'react'
import {Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavbarText
  } from 'reactstrap';
import {NavLink, Route, Switch} from 'react-router-dom'
import Search from "./Search"
import ListBrands from './ListBrands';
const Home = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
  
    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">Home</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                <Nav className="mr-auto" navbar>
                    <NavItem className="mr-2">
                        <NavLink to="/search">Search</NavLink>
                    </NavItem>
                    <NavItem className="mr-2">
                        <NavLink to="/brand">List Brand</NavLink>
                    </NavItem>
                </Nav>
                <NavbarText>Gerry & Giov</NavbarText>
                </Collapse>
            </Navbar>

            <Switch>
                <Route path="/search">
                    <Search />
                </Route>
                <Route path="/brand">
                    <ListBrands />
                </Route>
                <Route path="/">
                    <h1>Silahkan Mencari atau melihat brand hp</h1>
                </Route>
            </Switch>
        </div>
    )
}

export default Home
