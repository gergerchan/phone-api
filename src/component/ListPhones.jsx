import axios from "axios"
import { useEffect, useState } from "react"

const ListPhones = () => {
  const [listPhone, setListPhone] = useState(null)

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      const url = "http://api-mobilespecs.azharimm.tk/brands"
      await axios.get(`${url}/att`)
        .then(res => {
          // console.log(res);
          setListPhone(res.data.data.phones)
        })
    } catch (err) {
      console.log(err);
    }
  }
  console.log(listPhone);

  return (
    <>
      <h1>List Phone:</h1>
      {/* <Button onClick={getData}>GetData</Button> */}
      <ul>
        {listPhone && listPhone.map((data, i) => (
          <li key={i}>{data.phone_name}</li>
        ))}
      </ul>

    </>
  )
}

export default ListPhones
