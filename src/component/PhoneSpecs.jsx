import axios from "axios"
import { useEffect, useState } from "react"
import { Card, CardBody, CardImg, CardSubtitle, CardText, CardTitle } from "reactstrap"

const PhoneSpecs = () => {
  const [phoneSpecs, setPhoneSpecs] = useState(null)

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      const url = "http://api-mobilespecs.azharimm.tk/brands"
      await axios.get(`${url}/samsung/galaxy-note20-ultra-5g`)
        .then(res => {
          // console.log(res);
          setPhoneSpecs(res.data.data)
        })
    } catch (err) {
      console.log(err);
    }

  }
  console.log(phoneSpecs);

  return (
    <>
      <h1>Phone Specs:</h1>
      {phoneSpecs &&
        <div className="row">
          <div className="col-3">
            <Card>
              <CardImg top width="100%" src={phoneSpecs.phone_img_url} alt="Card image cap" />
              <CardBody>
                <CardTitle tag="h5">{phoneSpecs.phone_name}</CardTitle>
                <CardSubtitle tag="h6" className="mb-2 text-muted">Specification:</CardSubtitle>
                <CardText>{phoneSpecs.specifications[1].title}</CardText>
              </CardBody>
            </Card>
          </div>
        </div>
      }
    </>
  )
}

export default PhoneSpecs
