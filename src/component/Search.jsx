import { Button, Input,   Card, CardImg, CardBody, CardTitle,Label, FormGroup, Form} from "reactstrap"
import axios from 'axios'
import { useState } from "react"

const Search = () => {
    const [hasil, setHasil] = useState(null)
    const [nama, setNama] = useState(null)

    const findData = (e) => {
        e.preventDefault()
        const params = new URLSearchParams({
            query:nama
        })
        const url  = `https://api-mobilespecs.azharimm.tk/search?${params}`
        axios.get(url)
        .then(res =>{ 
            console.log(res.data);
            setHasil(res.data.data.phones)
        })
        .catch(err => console.log(err))
        
    }
    console.log(hasil);
    return (
        <>
            <h1>Search HP Impianmu</h1>
            <Form inline className="mb-2" onSubmit={findData}>
                <FormGroup>
                    <Label className="mr-2">Nama HP</Label>
                    <Input className="mr-2" type="text" name="title" placeholder="Masukkan Nama HP" onChange={(e) => setNama(e.target.value)} />
                </FormGroup>
                <Button type="submit"> Submit</Button>
            </Form>
            <div className="row">
            {hasil && hasil.map((data,i) => (
                <div key={i} className="col-3">
                    <Card >
                        <CardImg top width="100%" src={data.phone_img_url} alt="HP" />
                        <CardBody>
                        <CardTitle tag="h5">{data.phone_name}</CardTitle>
                        </CardBody>
                    </Card>
                </div> 
            ))}
            </div>
        </>
    )
}

export default Search
